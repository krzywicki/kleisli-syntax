package kleisliSyntax.v5

import scala.concurrent.{ExecutionContext, Future}
import scalaz.Id.Id
import scalaz.{-\/, EitherT, Functor, Kleisli, WriterT, \/-}

object model {

  type FutureRule[A, B] = Kleisli[Future, A, B]

  object FutureRule {
    def apply[A, B](f: A => Future[B]): FutureRule[A, B] = Kleisli(f)
  }

  // --- Validation ------

  case class Error(code: String, description: String)
  type ValidatedT[F[_], A] = EitherT[F, Vector[Error], A]
  val ValidatedT = EitherT
  type Validated[A] = ValidatedT[Id, A]

  type ValidatedDoc = Vector[Error]
  val ValidatedDoc = Vector
  case class ValidatedRuleT[F[_], A, B](run: A => ValidatedT[F, B], doc: ValidatedDoc)

  object ValidatedRuleT {
    import scalaz.syntax.functor._

    def create[F[_]: Functor, A, B](error: Error)(f: A => F[Option[B]]): ValidatedRuleT[F, A, B] = {
      ValidatedRuleT[F, A, B](
        a => ValidatedT {
          f(a).map {
            case None => -\/(Vector(error))
            case Some(b) => \/-(b)
          }
        },
        ValidatedDoc(error)
      )
    }

    def predicate[F[_]: Functor, A](error: Error)(p: A => F[Boolean]): ValidatedRuleT[F, A, A] = create(error) {
      a => p(a).map {
        case false => None
        case true => Some(a)
      }
    }

  }

  type ValidatedRule[A, B] = ValidatedRuleT[Id, A, B]
  object ValidatedRule {
    def create[A, B](error: Error)(f: A => Option[B]): ValidatedRule[A, B] =
      ValidatedRuleT.create[Id, A, B](error)(f)
    def predicate[A](error: Error)(p: A => Boolean): ValidatedRule[A, A] =
      ValidatedRuleT.predicate[Id, A](error)(p)
  }

  type FutureValidatedRule[A, B] = ValidatedRuleT[Future, A, B]
  object FutureValidatedRule {
    import scalaz.std.scalaFuture.futureInstance
    def create[A, B](error: Error)(f: A => Future[Option[B]])(implicit ec: ExecutionContext): FutureValidatedRule[A, B] =
      ValidatedRuleT.create[Future, A, B](error)(f)
    def predicate[A](error: Error)(p: A => Future[Boolean])(implicit ec: ExecutionContext): FutureValidatedRule[A, A] =
      ValidatedRuleT.predicate[Future, A](error)(p)
  }


  // --- Event Sourcing ------

  case class IsEvent[E](name: String)

  type SourcedT[F[_], A] = WriterT[F, Vector[Any], A]
  val SourcedT = WriterT
  type Sourced[A] = SourcedT[Id, A]

  type SourcedDoc = Vector[IsEvent[_]]
  val SourcedDoc = Vector

  case class SourcedRuleT[F[_], A, B](run: A => SourcedT[F, B], doc: SourcedDoc)
  object SourcedRuleT {
    import scalaz.syntax.functor._

    class CreatePartiallyApplied[A] {
      def apply[F[_]: Functor, E, B](f: A => F[E])(handler: (A, E) => B)(implicit isEvent: IsEvent[E]): SourcedRuleT[F, A, B] = SourcedRuleT(
        a => {
          SourcedT {
            f(a).map { e =>
              val b = handler(a, e)
              Vector(e) -> b
            }
          }
        },
        SourcedDoc(isEvent)
      )
    }

    def create[A]: CreatePartiallyApplied[A] = new CreatePartiallyApplied[A]

  }

  type SourcedRule[A, B] = SourcedRuleT[Id, A, B]
  object SourcedRule {
    class CreatePartiallyApplied[A] {
      def apply[E, B](f: A => E)(handler: (A, E) => B)(implicit isEvent: IsEvent[E]): SourcedRule[A, B] = {
        SourcedRuleT.create[A].apply[Id, E, B](f)(handler)
      }
    }
    def create[A]: CreatePartiallyApplied[A] = new CreatePartiallyApplied[A]
  }

  type FutureSourcedRule[A, B] = SourcedRuleT[Future, A, B]
  object FutureSourcedRule {
    import scalaz.std.scalaFuture.futureInstance
    class CreatePartiallyApplied[A] {
      def apply[E, B](f: A => Future[E])(handler: (A, E) => B)(implicit isEvent: IsEvent[E], ec: ExecutionContext): FutureSourcedRule[A, B] = {
        SourcedRuleT.create[A].apply[Future, E, B](f)(handler)
      }
    }
    def create[A]: CreatePartiallyApplied[A] = new CreatePartiallyApplied[A]
  }


  // --- Event Sourcing + Validation ------


  type CommandedT[F[_], B] = SourcedT[ValidatedT[F, ?], B]
  type Commanded[A] = CommandedT[Id, A]

  type CommandedDoc = (ValidatedDoc, SourcedDoc)
  case class CommandedRuleT[F[_], A, B](run: A => CommandedT[F, B], doc: CommandedDoc)
  type CommandedRule[A, B] = CommandedRuleT[Id, A, B]

}