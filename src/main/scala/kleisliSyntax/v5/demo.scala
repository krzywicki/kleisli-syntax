package kleisliSyntax.v5

import kleisliSyntax.v4.syntax._
import kleisliSyntax.v5.instances.all._
import kleisliSyntax.v5.model._
import kleisliSyntax.v5.transformations.all._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scalaz.Category

object demo extends App {

  case class FooEvent(value: Int)

  object FooEvent {
    implicit val isEvent: IsEvent[FooEvent] = IsEvent("FooEvent")
  }

  case class BooEvent(value: String)

  object BooEvent {
    implicit val isEvent: IsEvent[BooEvent] = IsEvent("BooEvent")
  }

  case object Lucky {
    implicit val isEvent: IsEvent[Lucky.type] = IsEvent("Lucky")
  }

//  identityF[ValidatedRule, Int] andThen identityF[ValidatedRule, Int]

//  val arrow = identity[Int].andThen {
//    (i: Int) => i * 2
//  }.to[ValidatedRule].andThen {
//    ValidatedRule.predicate[Int](Error("code1", "must be positive"))(a => a > 0)
//  }.to[CommandedRule].andThen {
//    SourcedRule.create[Int](value => FooEvent(value))( (value, event) => value + event.value )
//  }.andThen {
//    (i: Int) => i + 10
//  }.andThen{
//    when((_: Int) => math.random < 0.5) {
//      SourcedRule.create[Int](_ => Lucky)( (_, _) => 10000 )
//    }
//  }.andThen {
//    SourcedRule.create[Int](i => BooEvent(s"done with $i"))( (_, event) => event.value )
//  }.andThen {
//    (s: String) => Future(s)
//  }.andThen {
//    (s: String) => s ++ s
//  }

//  val arrow = new Ops[Int, Int, Function1T[Future, ?, ?]](identityF[Function1T[Future, ?, ?], Int]).andThen {
  //    (i: Int) => i *2
  //  }

  val arrow = identityF[FutureRule, Int].andThenM {
    (i: Int) => i * 2
  }.andThenM {
    FutureRule((i: Int) => Future("s"))
  }

//    .andThenM {
//    Kleisli.kleisliU((i: Int) => i * 2)
//  }

//    .andThenM {
//    (i: Int) => i * 2
//  }
  // .to[FutureValidatedRule]

//  println(
//    s"The arrow may produce any of the following errors: [${arrow.doc._1.map("'" ++ _.toString ++ "'").mkString(",")}], " +
//      s"or some of the following events: [${arrow.doc._2.mkString(",")}]"
//  )
//
//  Seq(-1, 0, 2).foreach {
//    i => println(s"Result for $i: ${arrow.run(i)}")
//  }

}
