package kleisliSyntax.v5

import kleisliSyntax.v4.lib
import kleisliSyntax.v5.instances.all._
import kleisliSyntax.v5.model.{FutureRule, _}

import scala.concurrent.ExecutionContext
import scalaz.Id.Id
import scalaz.Kleisli.kleisli
import scalaz.{BiNaturalTransformation, Monad, ~~>}

object transformations {

  trait SynchronousTransformations {
    
  }

  object all extends Instances5

  sealed trait InstancesK extends Instances1 {}

  sealed trait Instances5 extends Instances4 {
    import scalaz.WriterT.writerTMonad
    import scalaz.std.vector.vectorMonoid

    implicit def validatedTToCommandedT[F[_]: Monad]: ValidatedRuleT[F, ?, ?] ~~> CommandedRuleT[F, ?, ?] = {
      new ~~>[ValidatedRuleT[F, ?, ?], CommandedRuleT[F, ?, ?]] {

        def apply[A, B](f: ValidatedRuleT[F, A, B]): CommandedRuleT[F, A, B] = {
          CommandedRuleT[F, A, B](
            a => SourcedT(f.run(a).map(Monad[SourcedT[Id, ?]].pure(_).run)),
            (f.doc, Vector.empty)
          )
        }
      }
    }

    implicit def sourcedTToCommandedT[F[_]: Monad]: SourcedRuleT[F, ?, ?] ~~> CommandedRuleT[F, ?, ?] = new BiNaturalTransformation[SourcedRuleT[F, ?, ?], CommandedRuleT[F, ?, ?]] {
      def apply[A, B](f: SourcedRuleT[F, A, B]): CommandedRuleT[F, A, B] = {
        kleisli[SourcedT[F, ?], A, B](f.run)

        // =>
        // Kleisli[WriterT[SourcedT[

        val xxx = (a: A) => {
          val value: SourcedT[F, B] = f.run(a)

        }
        CommandedRuleT[F, A, B](
          a => f.run(a).mapT(ValidatedT.right(_)),
          (Vector.empty, f.doc)
        )
      }
    }
  }

  sealed trait Instances4 extends Instances3 {
    // future
  }

  sealed trait Instances3 extends Instances2 {
    implicit def functionToSourcedT[F[_] : Monad]: Function1 ~~> SourcedRuleT[F, ?, ?] = lib.functionToArrow[SourcedRuleT[F, ?, ?]]
  }

  sealed trait Instances2 extends Instances1 {
    // future
  }

  sealed trait Instances1 extends Instances01 {
    implicit def functionToValidatedT[F[_] : Monad]: Function1 ~~> ValidatedRuleT[F, ?, ?] = lib.functionToArrow[ValidatedRuleT[F, ?, ?]]
//    implicit def futurefunctionToValidatedTimplicit ec: ExecutionContext)[F[_] : Monad]: Function1 ~~> ValidatedRuleT[F, ?, ?] = lib.functionToArrow[ValidatedRuleT[F, ?, ?]]
  }

  sealed trait Instances01 extends Instances0 {
    implicit def functionToFutureRule(implicit ec: ExecutionContext): Function1 ~~> FutureRule = lib.functionToArrow[FutureRule]
  }

  sealed trait Instances0 extends Instances00 {
    implicit def identityNatTrans[F[_, _]]: F ~~> F = lib.identityNatTrans[F]
  }

  sealed trait Instances00  {
    implicit def composedNatTrans[F[_, _], G[_, _], H[_, _]](implicit FG: F ~~> G, GH: G ~~> H): F ~~> H = GH compose FG
  }

}
