package kleisliSyntax.v5
import kleisliSyntax.v4.lib.{ArrowChoice, DocumentedArrowInstance}
import kleisliSyntax.v5.model.{FutureRule, _}

import scala.concurrent.{ExecutionContext, Future}
import scalaz.Monad

object instances {

  trait FutureInstances {
    import scalaz.std.scalaFuture.futureInstance
    import scalaz.Monoid

    implicit def futureRuleIsArrow(implicit ec: ExecutionContext): ArrowChoice[FutureRule] = {
      implicit val unitMonoid: Monoid[Unit] = Monoid.instance[Unit]((_, _) => (), ())

      new DocumentedArrowInstance[FutureRule, Future, Unit] {

        def apply[A, B](run: A => Future[B], doc: Unit): FutureRule[A, B] = FutureRule(run)

        def unapply[A, B](arrow: FutureRule[A, B]): (A => Future[B], Unit) = (arrow, ())
      }
    }
  }

  trait ValidatedInstances {
    import scalaz.std.vector.vectorMonoid

    implicit def validatedRuleTIsArrow[F[_]: Monad]: ArrowChoice[ValidatedRuleT[F, ?, ?]] = {
      new DocumentedArrowInstance[ValidatedRuleT[F, ?, ?], ValidatedT[F, ?], ValidatedDoc] {

        def apply[A, B](run: A => ValidatedT[F, B], doc: ValidatedDoc): ValidatedRuleT[F, A, B] = ValidatedRuleT(run, doc)

        def unapply[A, B](arrow: ValidatedRuleT[F, A, B]): (A => ValidatedT[F, B], ValidatedDoc) = (arrow.run, arrow.doc)
      }
    }
  }

  trait SourcedInstances {
    import scalaz.std.vector.vectorMonoid

    implicit def sourcedRuleTIsArrow[F[_]: Monad]: ArrowChoice[SourcedRuleT[F, ?, ?]] = {
      new DocumentedArrowInstance[SourcedRuleT[F, ?, ?], SourcedT[F, ?], SourcedDoc] {

        def apply[A, B](run: A => SourcedT[F, B], doc: SourcedDoc): SourcedRuleT[F, A, B] = SourcedRuleT(run, doc)

        def unapply[A, B](arrow: SourcedRuleT[F, A, B]): (A => SourcedT[F, B], SourcedDoc) = (arrow.run, arrow.doc)
      }
    }
  }

  trait CommandedInstances {
    import scalaz.std.vector.vectorMonoid
    import scalaz.std.tuple.tuple2Monoid

    implicit def commandedRuleTIsArrow[F[_]: Monad]: ArrowChoice[CommandedRuleT[F, ?, ?]] = {
      new DocumentedArrowInstance[CommandedRuleT[F, ?, ?], CommandedT[F, ?], CommandedDoc] {

        def apply[A, B](run: A => CommandedT[F, B], doc: CommandedDoc): CommandedRuleT[F, A, B] = CommandedRuleT(run, doc)

        def unapply[A, B](arrow: CommandedRuleT[F, A, B]): (A => CommandedT[F, B], CommandedDoc) = (arrow.run, arrow.doc)
      }
    }

  }

  object all extends FutureInstances with ValidatedInstances with SourcedInstances with CommandedInstances

}
