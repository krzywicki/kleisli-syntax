package kleisliSyntax.v4

import kleisliSyntax.v4.lib._
import kleisliSyntax.v4.model._

import scalaz.{Monad, Monoid, \/-}

object instances {

  trait ValidatedInstances {

    implicit val validatedDocIsMonoid: Monoid[ValidatedDoc] = new Monoid[ValidatedDoc] {
      def zero: ValidatedDoc = ValidatedDoc(Vector.empty)

      def append(f1: ValidatedDoc, f2: => ValidatedDoc): ValidatedDoc = {
        ValidatedDoc(possibleErrors = (f1.possibleErrors ++ f2.possibleErrors).distinct)
      }
    }

    implicit val validatedIsMonad: Monad[Validated] = new Monad[Validated] {
      def point[A](a: => A): Validated[A] = Validated(\/-(a))

      def bind[A, B](fa: Validated[A])(f: A => Validated[B]): Validated[B] = {
        Validated(
          fa.run.flatMap(a => f(a).run)
        )
      }
    }

    implicit val validatedRuleIsArrow: ArrowChoice[ValidatedRule] = new DocumentedArrowInstance[ValidatedRule, Validated, ValidatedDoc] {

      def apply[A, B](run: A => Validated[B], doc: ValidatedDoc): ValidatedRule[A, B] = ValidatedRule(run, doc)

      def unapply[A, B](arrow: ValidatedRule[A, B]): (A => Validated[B], ValidatedDoc) = (arrow.run, arrow.doc)

    }
  }

  trait SourcedInstances {

    implicit val sourcedDocIsMonoid: Monoid[SourcedDoc] = new Monoid[SourcedDoc] {
      def zero: SourcedDoc = SourcedDoc(Vector.empty)

      def append(f1: SourcedDoc, f2: => SourcedDoc): SourcedDoc = {
        SourcedDoc(possibleEvents = (f1.possibleEvents ++ f2.possibleEvents).distinct)
      }
    }

    implicit val sourcedIsMonad: Monad[Sourced] = new Monad[Sourced] {
      def point[A](a: => A): Sourced[A] = Sourced((Vector.empty, a))

      def bind[A, B](fa: Sourced[A])(f: A => Sourced[B]): Sourced[B] = {
        Sourced {
          val (oldEvents, oldState) = fa.run
          val (newEvents, newState) = f(oldState).run

          (oldEvents ++ newEvents) -> newState
        }
      }
    }

    implicit val sourcedRuleIsArrow: ArrowChoice[SourcedRule] = new DocumentedArrowInstance[SourcedRule, Sourced,
      SourcedDoc] {

      def apply[A, B](run: A => Sourced[B], doc: SourcedDoc): SourcedRule[A, B] = SourcedRule(run, doc)

      def unapply[A, B](arrow: SourcedRule[A, B]): (A => Sourced[B], SourcedDoc) = (arrow.run, arrow.doc)
    }
  }

  trait CommandedInstances {

    implicit val commandedDocIsMonoid: Monoid[CommandedDoc] = new Monoid[CommandedDoc] {
      def zero: CommandedDoc = CommandedDoc(Vector.empty, Vector.empty)

      def append(f1: CommandedDoc, f2: => CommandedDoc): CommandedDoc = {
        CommandedDoc(
          possibleErrors = (f1.possibleErrors ++ f2.possibleErrors).distinct,
          possibleEvents = (f1.possibleEvents ++ f2.possibleEvents).distinct
        )
      }
    }

    implicit val commandedIsMonad: Monad[Commanded] = new Monad[Commanded] {
      def point[A](a: => A): Commanded[A] = Commanded(\/-(Vector.empty, a))

      def bind[A, B](fa: Commanded[A])(f: A => Commanded[B]): Commanded[B] = {
        Commanded {
          fa.run.flatMap { case (oldEvents, oldState) =>
            f(oldState).run.map { case (newEvents, newState) =>
              (oldEvents ++ newEvents) -> newState
            }
          }
        }
      }
    }

    implicit val commandedRuleIsArrow: ArrowChoice[CommandedRule] = new DocumentedArrowInstance[CommandedRule, Commanded,
      CommandedDoc] {

      def apply[A, B](run: A => Commanded[B], doc: CommandedDoc): CommandedRule[A, B] = CommandedRule(run, doc)

      def unapply[A, B](arrow: CommandedRule[A, B]): (A => Commanded[B], CommandedDoc) = (arrow.run, arrow.doc)
    }

  }

  object all
    extends ValidatedInstances
      with SourcedInstances
      with CommandedInstances

}
