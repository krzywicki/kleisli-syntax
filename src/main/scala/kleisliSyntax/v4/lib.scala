package kleisliSyntax.v4

import scalaz.Kleisli.kleisli
import scalaz.{Arrow, BiNaturalTransformation, Choice, Kleisli, Monad, Monoid, ProChoice, \/, ~~>}

object lib {

  trait ArrowChoice[=>:[_, _]] extends Arrow[=>:] with Choice[=>:] with ProChoice[=>:] {
    def cosplit[A, B, C, D](first: A =>: B, second: C =>: D): (A \/ C) =>: (B \/ D) = {
      compose(right[C, D, B](second), left[A, B, C](first))
    }

    def coproduct[A, B](fab: (A =>: B)): (A \/ A) =>: (B \/ B) = {
      cosplit(fab, fab)
    }
  }

  abstract class DocumentedArrowInstance[=>:[_, _], F[_]: Monad, D: Monoid] extends ArrowChoice[=>:] {

    def apply[A, B](run: A => F[B], doc: D): A =>: B

    def unapply[A, B](arrow: A =>: B): (A => F[B], D)

    final def arr[A, B](f: A => B): A =>: B = {
      apply(
        Arrow[Kleisli[F, ?, ?]].arr(f).run,
        Monoid[D].zero
      )
    }

    final def id[A]: A =>: A = arr(identity)

    final def first[A, B, C](fa: A =>: B): (A, C) =>: (B, C) = {
      val (run, doc) = unapply(fa)
      apply(
        Arrow[Kleisli[F, ?, ?]].first[A, B, C](kleisli(run)).run,
        doc
      )
    }

    final def compose[A, B, C](f: B =>: C, g: A =>: B): A =>: C = {
      val (runF, docF) = unapply(f)
      val (runG, docG) = unapply(g)

      apply(
        Arrow[Kleisli[F, ?, ?]].compose[A, B, C](kleisli(runF), kleisli(runG)).run,
        Monoid[D].append(docF, docG)
      )
    }

    def choice[A, B, C](f: => A =>: C, g: => B =>: C): \/[A, B] =>: C = {
      val (runF, docF) = unapply(f)
      val (runG, docG) = unapply(g)
      apply(
        Choice[Kleisli[F, ?, ?]].choice[A, B, C](kleisli(runF), kleisli(runG)).run,
        Monoid[D].append(docF, docG)
      )
    }

    def left[A, B, C](fa: A =>: B): (A \/ C) =>: (B \/ C) = {
      val (runF, docF) = unapply(fa)
      apply(
        ProChoice[Kleisli[F, ?, ?]].left[A, B, C](kleisli(runF)).run,
        docF
      )
    }

    def right[A, B, C](fa: A =>: B): (C \/ A) =>: (C \/ B) = {
      val (runF, docF) = unapply(fa)
      apply(
        ProChoice[Kleisli[F, ?, ?]].right[A, B, C](kleisli(runF)).run,
        docF
      )
    }
  }

  def identityNatTrans[F[_, _]]: F ~~> F = new BiNaturalTransformation[F, F] {
    def apply[A, B](f: F[A, B]): F[A, B] = f
  }

  def functionToArrow[F[_, _]](implicit isArrow: Arrow[F]): Function1 ~~> F = {
    new BiNaturalTransformation[Function1, F] {

      def apply[A, B](f: A => B): F[A, B] = isArrow.arr(f)
    }
  }

}
