package kleisliSyntax.v4

import kleisliSyntax.v4.lib.ArrowChoice

import scala.language.implicitConversions
import scalaz.{-\/, Arrow, Category, Profunctor, \/, \/-, ~~>}

object syntax {

  implicit private def identityNatTrans[F[_, _]]: F ~~> F = lib.identityNatTrans[F]

  // Natural transformations syntax

  def identity[A]: A => A = a => a

  def identityF[F[_, _], A](implicit toF: Function1 ~~> F): F[A, A] = toF(x => x)

  final class AndThenMPartiallyApplied[A, B, F[_, _], G[_, _]](self: F[A, B]) {

    def apply[C](g: G[B, C])(implicit toF: G ~~> F, isCat: Category[F]): F[A, C] = {
      isCat.compose(toF(g), self)
    }

  }

  implicit class Ops[A, B, F[_, _]](val self: F[A, B]) extends AnyVal {

    // Natural transformation syntax

    def to[G[_, _]](implicit toG: F ~~> G): G[A, B] = toG(self)

    // Compose syntax

    def andThen[C, G[_, _]](g: G[B, C])(implicit toF: G ~~> F, isCat: Category[F]): F[A, C] = andThenM[G](g)

    def andThenM[G[_, _]]: AndThenMPartiallyApplied[A, B, F, G] = new AndThenMPartiallyApplied(self)

    // Profunctor syntax

    def cmap[C](f: C => A)(implicit F: Profunctor[F]): F[C, B] = F.mapfst(self)(f)

    def map[C](f: B => C)(implicit F: Profunctor[F]): F[A, C] = F.mapsnd(self)(f)

    def inmap[C, D](f: C => A, g: B => D)(implicit F: Profunctor[F]): F[C, D] = F.dimap(self)(f)(g)

    // Arrow syntax

    def first[C](implicit F: Arrow[F]): F[(A, C), (B, C)] = F.first(self)

    def second[C](implicit F: Arrow[F]): F[(C, A), (C, B)] = F.second(self)

    def product(implicit F: Arrow[F]): F[(A, A), (B, B)] = F.product(self)

    def split[C, D, G[_, _]](other: G[C, D])(implicit toF: G ~~> F, F: Arrow[F]): F[(A, C), (B, D)] = {
      F.split(self, toF(other))
    }

    def combine[C, G[_, _]](other: G[A, C])(implicit toF: G ~~> F, F: Arrow[F]): F[A, (B, C)] = {
      F.combine(self, toF(other))
    }

    // Choice syntax

    def left[C](implicit F: ArrowChoice[F]): F[(A \/ C), (B \/ C)] = F.left(self)

    def right[C](implicit F: ArrowChoice[F]): F[(C \/ A), (C \/ B)] = F.right(self)

    def coproduct(implicit F: ArrowChoice[F]): F[(A \/ A), (B \/ B)] = F.coproduct(self)

    def cosplit[C, D, G[_, _]](other: G[C, D])(implicit toF: G ~~> F, F: ArrowChoice[F]): F[A \/ C, B \/ D] = {
      F.cosplit(self, toF(other))
    }

    def choice[A2, G[_, _]](other: G[A2, B])(implicit toF: G ~~> F, F: ArrowChoice[F]): F[A \/ A2, B] = {
      F.choice(self, toF(other))
    }

    // Additional combinators

    def keepAnd[C, G[_, _]](other: G[A, C])(implicit toF: G ~~> F, F: Arrow[F]): F[A, B] = {
      combine(other).map(_._1)
    }

    def andKeep[C, G[_, _]](other: G[A, C])(implicit toF: G ~~> F, F: Arrow[F]): F[A, C] = {
      combine(other).map(_._2)
    }

  }

  // Branching syntax

  /**
   * Transforms a predicate arrow into an arrow returning:
   * - the input wrapped in a Left is the predicate is proven true
   * - the input wrapped in a Right if the predicate is proven false.
   */
  private def test[A, F[_, _]](self: F[A, Boolean])(implicit F: Arrow[F]): F[A, A \/ A] = {
    self
      .combine(F.arr(identity[A])) // we add the input unchanged: F[A, (Boolean, A)]
      .andThen(F.arr[(Boolean, A), A \/ A] {
        case (true, a) => -\/(a)
        case (false, a) => \/-(a)
      })
  }

  def check[A, B, C, F[_, _], P[_, _]](
    p: P[A, Boolean]
  )(
    whenTrue: F[A, B],
    whenFalse: F[A, C]
  )(implicit toF: P ~~> F, F: ArrowChoice[F]): F[A, B \/ C] = {
    test(toF(p)) andThen F.cosplit(whenTrue, whenFalse)
  }

  def when[A, F[_, _], P[_, _]](
    p: P[A, Boolean]
    )(
    whenTrue: F[A, A]
  )(implicit toF: P ~~> F, F: ArrowChoice[F]): F[A, A] = {
    check(p)(whenTrue, whenFalse = F.arr(identity[A])) andThen {
      F.arr[A \/ A, A](_.merge)
    }
  }

}
