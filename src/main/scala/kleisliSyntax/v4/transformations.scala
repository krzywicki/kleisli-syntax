package kleisliSyntax.v4

import kleisliSyntax.v4.instances.all._
import kleisliSyntax.v4.model._

import scalaz.{BiNaturalTransformation, Monad, \/-, ~~>}

object transformations {

  object all extends Instances3

  sealed trait Instances3 extends Instances2 {
    implicit val validatedToCommanded: ValidatedRule ~~> CommandedRule = new BiNaturalTransformation[ValidatedRule, CommandedRule] {
      def apply[A, B](fa: ValidatedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B](
        a => Commanded(fa.run(a).run.map(Monad[Sourced].pure(_).run)),
        CommandedDoc(possibleEvents = Vector.empty, possibleErrors = fa.doc.possibleErrors)
      )
    }

    implicit val sourcedToCommanded: SourcedRule ~~> CommandedRule = new BiNaturalTransformation[SourcedRule, CommandedRule] {
      def apply[A, B](fa: SourcedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B](
        a => Commanded(\/-(fa.run(a).run)),
        CommandedDoc(possibleEvents = fa.doc.possibleEvents, possibleErrors = Vector.empty)
      )
    }
  }

  sealed trait Instances2 extends Instances1 {
    implicit val functionToSourced: Function1 ~~> SourcedRule = lib.functionToArrow[SourcedRule]
  }

  sealed trait Instances1 extends Instances0 {
    implicit val functionToValidated: Function1 ~~> ValidatedRule = lib.functionToArrow[ValidatedRule]
  }

  sealed trait Instances0  {
    implicit def identityNatTrans[F[_, _]]: F ~~> F = lib.identityNatTrans[F]

    implicit def composedNatTrans[F[_, _], G[_, _], H[_, _]](implicit FG: F ~~> G, GH: G ~~> H): F ~~> H = GH compose FG


  }

}
