package kleisliSyntax.v4

import scalaz.{-\/, \/, \/-}

object model {

  // --- Validation ------

  case class Validated[A](run: \/[Vector[String], A])

  case class ValidatedDoc(possibleErrors: Vector[String])

  case class ValidatedRule[A, B](run: A => Validated[B], doc: ValidatedDoc)

  object ValidatedRule {

    def create[A, B](error: String)(f: A => Option[B]): ValidatedRule[A, B] = {
      ValidatedRule[A, B](
        a => Validated {
          f(a) match {
            case None => -\/(Vector(error))
            case Some(b) => \/-(b)
          }
        },
        ValidatedDoc(Vector(error))
      )
    }

    def predicate[A](error: String)(p: A => Boolean): ValidatedRule[A, A] = create(error) {
      a => Some(a).filter(p)
    }

  }

  // --- Event Sourcing ------

  case class IsEvent[E](name: String)

  case class Sourced[A](run: (Vector[Any], A))

  case class SourcedDoc(possibleEvents: Vector[String])

  case class SourcedRule[A, B](run: A => Sourced[B], doc: SourcedDoc)

  object SourcedRule {

    class CreatePartiallyApplied[A] {
      def apply[E, B](f: A => E)(handler: (A, E) => B)(implicit isEvent: IsEvent[E]): SourcedRule[A, B] = SourcedRule(
        a => {
          val e = f(a)
          val b = handler(a, e)
          Sourced(Vector(e) -> b)
        },
        SourcedDoc(Vector(isEvent.name))
      )
    }

    def create[A]: CreatePartiallyApplied[A] = new CreatePartiallyApplied[A]

  }

  // --- Event Sourcing + Validation ------


  case class Commanded[A](run: \/[Vector[String], (Vector[Any], A)])

  case class CommandedDoc(possibleEvents: Vector[String], possibleErrors: Vector[String])

  case class CommandedRule[A, B](run: A => Commanded[B], doc: CommandedDoc)

}