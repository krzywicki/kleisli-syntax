package kleisliSyntax.v4

import kleisliSyntax.v4.instances.all._
import kleisliSyntax.v4.model._
import kleisliSyntax.v4.syntax._
import kleisliSyntax.v4.transformations.all._

object demo extends App {

  case class FooEvent(value: Int)

  object FooEvent {
    implicit val isEvent: IsEvent[FooEvent] = IsEvent("FooEvent")
  }

  case class BooEvent(value: String)

  object BooEvent {
    implicit val isEvent: IsEvent[BooEvent] = IsEvent("BooEvent")
  }

  case object Lucky {
    implicit val isEvent: IsEvent[Lucky.type ] = IsEvent("Lucky")
  }

  identityF[ValidatedRule, Int] andThen identityF[ValidatedRule, Int]

  val arrow = identity[Int].andThen {
    (i: Int) => i * 2
  }.to[ValidatedRule].andThen {
    ValidatedRule.predicate[Int]("must be positive")(a => a > 0)
  }.to[CommandedRule].andThen {
    SourcedRule.create[Int](value => FooEvent(value))( (value, event) => value + event.value )
  }.andThen {
    (i: Int) => i + 10
  }.andThen{
    when((_: Int) => math.random < 0.5) {
      SourcedRule.create[Int](_ => Lucky)( (_, _) => 10000 )
    }
  }.andThen {
    SourcedRule.create[Int](i => BooEvent(s"done with $i"))( (_, event) => event.value )
  }

  println(
    s"The arrow may produce any of the following errors: [${arrow.doc.possibleErrors.map("'" ++ _ ++ "'").mkString(",")}], " +
      s"or some of the following events: [${arrow.doc.possibleEvents.mkString(",")}]"
  )

  Seq(-1, 0, 2).foreach {
    i => println(s"Result for $i: ${arrow.run(i)}")
  }

}
