package kleisliSyntax.v1_2_3

import scala.language.higherKinds
import scalaz.Kleisli._
import scalaz.std.function._
import scalaz.{-\/, Arrow, BiNaturalTransformation, Category, Kleisli, Monad, Monoid, \/-, ~~>}

object V3 extends App {

  // -- Building blocks

  abstract class DocumentedArrowInstance[=>:[_, _], F[_]: Monad, D: Monoid] extends Arrow[=>:] {

    def apply[A, B](run: A => F[B], doc: D): A =>: B

    def unapply[A, B](arrow: A =>: B): (A => F[B], D)

    def arr[A, B](f: A => B): A =>: B = {
      apply(
        Arrow[Kleisli[F, ?, ?]].arr(f).run,
        Monoid[D].zero
      )
    }

    def id[A]: A =>: A = arr(identity)

    def first[A, B, C](fa: A =>: B): (A, C) =>: (B, C) = {
      val (run, doc) = unapply(fa)
      apply(
        Arrow[Kleisli[F, ?, ?]].first[A, B, C](kleisli(run)).run,
        doc
      )
    }

    def compose[A, B, C](f: B =>: C, g: A =>: B): A =>: C= {
      val (runF, docF) = unapply(f)
      val (runG, docG) = unapply(g)

      apply(
        Arrow[Kleisli[F, ?, ?]].compose[A, B, C](kleisli(runF), kleisli(runG)).run,
        Monoid[D].append(docF, docG)
      )
    }

  }

  // -- Domain

  case class ValidatedRule[A, B](run: A => Validated[B], doc: ValidatedDoc)

  object ValidatedRule {

    def create[A, B](error: String)(f: A => Option[B]): ValidatedRule[A, B] = {
      ValidatedRule[A, B](
        a =>  Validated {
            f(a) match {
              case None => -\/(Vector(error))
              case Some(b) => \/-(b)
            }
          },
        ValidatedDoc.single(error)
      )
    }

    def predicate[A](error: String)(p: A => Boolean): ValidatedRule[A, A] = create(error) {
      a => Some(a).filter(p)
    }

  }

  case class IsEvent[E](name: String)

  case class SourcedRule[A, B](run: A => Sourced[B], doc: SourcedDoc)

  object SourcedRule {

    class CreatePartiallyApplied[A] {
      def apply[E, B](f: A => E)(handler: (A, E) => B)(implicit isEvent: IsEvent[E]): SourcedRule[A, B] = SourcedRule(
        a => {
          val e = f(a)
          val b = handler(a, e)
          Sourced(Vector(e) -> b)
        },
        SourcedDoc.single(isEvent.name)
      )
    }

    def create[A]: CreatePartiallyApplied[A] = new CreatePartiallyApplied[A]

  }

  case class CommandedRule[A, B](run: A => Commanded[B], doc: CommandedDoc)

  // -- Syntax

  object Syntax {

    def identity[A]: A => A = a => a

    def identityF[F[_, _], A](implicit toF: Function1 ~~> F): F[A, A] = toF(x => x)

    class AndThenMPartiallyApplied[A, B, F[_, _], G[_, _]](self: F[A, B]) {

      def apply[C](g: G[B, C])(implicit toF: G ~~> F, isCat: Category[F]): F[A, C] = {
        isCat.compose(toF(g), self)
      }

    }

    implicit class Ops[A, B, F[_, _]](val self: F[A, B]) {

      def andThen[C, G[_, _]](g: G[B, C])(implicit toF: G ~~> F, isCat: Category[F]): F[A, C] = andThenM[G](g)

      def andThenM[G[_, _]]: AndThenMPartiallyApplied[A, B, F, G] = new AndThenMPartiallyApplied(self)

      def to[G[_, _]](implicit toG: F ~~> G): G[A, B] = toG(self)
    }

  }
  // -- Algebra

  object Algebra {

    object Instances extends Instances3

    sealed trait Instances3 extends Instances2 {

      implicit val validatedToCommanded: ValidatedRule ~~> CommandedRule = new BiNaturalTransformation[ValidatedRule, CommandedRule] {
        def apply[A, B](fa: ValidatedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B](
          a => Commanded(fa.run(a).run.map(Sourced.pure(_).run)),
          CommandedDoc(possibleEvents = Vector.empty, possibleErrors = fa.doc.possibleErrors)
        )
      }

      implicit val eventedToCommanded: SourcedRule ~~> CommandedRule = new BiNaturalTransformation[SourcedRule, CommandedRule] {
        def apply[A, B](fa: SourcedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B](
          a => Commanded(\/-(fa.run(a).run)),
          CommandedDoc(possibleEvents = fa.doc.possibleEvents, possibleErrors = Vector.empty)
        )
      }

      implicit val commandedArrowInstance: Arrow[CommandedRule] = new DocumentedArrowInstance[CommandedRule, Commanded, CommandedDoc] {

        def apply[A, B](run: A => Commanded[B], doc: CommandedDoc): CommandedRule[A, B] = CommandedRule(run, doc)

        def unapply[A, B](arrow: CommandedRule[A, B]): (A => Commanded[B], CommandedDoc) = (arrow.run, arrow.doc)
      }

    }

    sealed trait Instances2 extends Instances1 {

      implicit val functionToValidated: Function1 ~~> ValidatedRule = new BiNaturalTransformation[Function1, ValidatedRule] {
        def apply[A, B](fa: Function1[A, B]): ValidatedRule[A, B] = ValidatedRule[A, B](
          a => Validated.pure(fa(a)),
          ValidatedDoc.empty
        )
      }


      implicit val validatedArrowInstance: Arrow[ValidatedRule] = new DocumentedArrowInstance[ValidatedRule, Validated, ValidatedDoc] {

        def apply[A, B](run: A => Validated[B], doc: ValidatedDoc): ValidatedRule[A, B] = ValidatedRule(run, doc)

        def unapply[A, B](arrow: ValidatedRule[A, B]): (A => Validated[B], ValidatedDoc) = (arrow.run, arrow.doc)
      }

    }

    sealed trait Instances1 extends Instances0 {
      implicit val functionToEvented: Function1 ~~> SourcedRule = new BiNaturalTransformation[Function1, SourcedRule] {
        def apply[A, B](fa: Function1[A, B]): SourcedRule[A, B] = SourcedRule[A, B](
          a => Sourced.pure(fa(a)),
          SourcedDoc.empty
        )
      }

      implicit val sourcedArrowInstance: Arrow[SourcedRule] = new DocumentedArrowInstance[SourcedRule, Sourced, SourcedDoc] {

        def apply[A, B](run: A => Sourced[B], doc: SourcedDoc): SourcedRule[A, B] = SourcedRule(run, doc)

        def unapply[A, B](arrow: SourcedRule[A, B]): (A => Sourced[B], SourcedDoc) = (arrow.run, arrow.doc)
      }

    }

    sealed trait Instances0 {

      implicit def identityNatTrans[F[_, _]]: F ~~> F = new BiNaturalTransformation[F, F] {
        def apply[A, B](f: F[A, B]): F[A, B] = f
      }

      implicit def composedNatTrans[F[_, _], G[_, _], H[_, _]](implicit FG: F ~~> G, GH: G ~~> H): F ~~> H = GH compose FG
    }
  }

  // Tests

  import Algebra.Instances._
  import Syntax._

  case class FooEvent(value: Int)

  object FooEvent {
    implicit val isEvent: IsEvent[FooEvent] = IsEvent("FooEvent")
  }

  case class BooEvent(value: Int)

  object BooEvent {
    implicit val isEvent: IsEvent[BooEvent] = IsEvent("BooEvent")
  }

  def function = (s: String) => 1

  val validatedRule: ValidatedRule[String, Int] = function.to[ValidatedRule]

  val eventedRule: SourcedRule[String, Int] = function.to[SourcedRule]

  val commandedRule: CommandedRule[String, Int] = function.to[CommandedRule]
  val commandedRule2: CommandedRule[String, Int] = validatedRule.to[CommandedRule]
  val commandedRule3: CommandedRule[String, Int] = eventedRule.to[CommandedRule]

  identityF[ValidatedRule, Int] andThen identityF[ValidatedRule, Int]

  val rule = identity[Int].andThen {
    (i: Int) => i * 2
  }.to[ValidatedRule].andThen {
    ValidatedRule.predicate[Int]("must be positive")(a => a > 0)
  }.to[CommandedRule].andThen {
    SourcedRule.create[Int](value => FooEvent(value))( (value, event) => value + event.value )
  }.andThen {
    (i: Int) => i + 10
  }.andThen {
    SourcedRule.create[Int](value => BooEvent(value))( (value, event) => "done" )
  }

  println(
    s"The arrow may produce any of the following errors: [${rule.doc.possibleErrors.map("'" ++ _ ++ "'").mkString(",")}], " +
    s"or all of the following events: [${rule.doc.possibleEvents.mkString(",")}]"
  )

  Seq(-1, 0, 2).foreach {
    i => println(s"Result for $i: ${rule.run(i)}")
  }


}


