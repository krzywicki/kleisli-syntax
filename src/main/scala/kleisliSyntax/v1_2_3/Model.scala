package kleisliSyntax.v1_2_3

import scalaz.{Monad, \/, \/-}

case class Validated[A](run: \/[Vector[String], A])

object Validated {
  def pure[A](a: A): Validated[A] = monadInstance.pure(a)

  implicit val monadInstance: Monad[Validated] = new Monad[Validated] {
    def point[A](a: => A) = Validated(\/-(a))

    def bind[A, B](fa: Validated[A])(f: A => Validated[B]) = Validated(
      fa.run.flatMap(a => f(a).run)
    )
  }
}

case class Sourced[A](run: (Vector[Any], A))

object Sourced {
  def pure[A](a: A): Sourced[A] = monadInstance.pure(a)

  implicit val monadInstance: Monad[Sourced] = new Monad[Sourced] {
    def point[A](a: => A) = Sourced((Vector.empty, a))

    def bind[A, B](fa: Sourced[A])(f: A => Sourced[B]) = Sourced {
      val (oldEvents, oldState) = fa.run
      val (newEvents, newState) = f(oldState).run

      (oldEvents ++ newEvents) -> newState
    }
  }
}

case class Commanded[A](run: \/[Vector[String], (Vector[Any], A)])

object Commanded {
  def pure[A](a: A): Commanded[A] = monadInstance.pure(a)

  implicit val monadInstance: Monad[Commanded] = new Monad[Commanded] {
    def point[A](a: => A) = Commanded(\/-(Vector.empty, a))

    def bind[A, B](fa: Commanded[A])(f: A => Commanded[B]) = Commanded {
      fa.run.flatMap { case (oldEvents, oldState) =>
        f(oldState).run.map { case (newEvents, newState) =>
          (oldEvents ++ newEvents) -> newState
        }
      }
    }
  }
}
