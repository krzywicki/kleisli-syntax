package kleisliSyntax.v1_2_3

import scala.language.higherKinds
import scalaz.{-\/, BiNaturalTransformation, \/-, ~~>}
object V1 {

  // -- Building blocks

  trait CompanionOps[G[_, _]] {

    def identity[A](implicit FG: Function1 ~~> G): G[A, A] = FG(x => x)

    //    def of[A]: OfPartiallyApplied[A] = new OfPartiallyApplied[A]
    //
    //    class OfPartiallyApplied[A] {
    //      def apply[B](f: A => B)(implicit FG: Function1 ~~> G): G[A, B] = FG(f)
    //
    //    }

  }

  // -- Domain

  case class ValidatedRule[A, B](run: A => Validated[B])

  object ValidatedRule extends CompanionOps[ValidatedRule]

  case class SourcedRule[A, B](run: A => Sourced[B])

  object SourcedRule extends CompanionOps[SourcedRule]

  case class CommandedRule[A, B](run: A => Commanded[B])

  object CommandedRule extends CompanionOps[CommandedRule]

  // -- Syntax

  object Syntax {

    class AndThenMPartiallyApplied[A, B, F[_, _], G[_, _]](self: F[A, B]) {

      def apply[C](g: G[B, C])(implicit FG: G ~~> F): F[A, C] = ???

    }

    implicit class Ops[A, B, F[_, _]](val self: F[A, B]) {

      def andThen[C, G[_, _]](g: G[B, C])(implicit FG: G ~~> F): F[A, C] = andThenM[G](g)

      def andThenM[G[_, _]]: AndThenMPartiallyApplied[A, B, F, G] = new AndThenMPartiallyApplied(self)

      def to[G[_, _]](implicit FG: F ~~> G): G[A, B] = {
        ???
      }
    }

  }
  // -- Algebra

  object Algebra {

    object Instances extends Instances3

    sealed trait Instances3 extends Instances2 {

      implicit val validatedToCommanded: ValidatedRule ~~> CommandedRule = new BiNaturalTransformation[ValidatedRule, CommandedRule] {
        def apply[A, B](fa: ValidatedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B] {
          a => Commanded(fa.run(a).run.map(Sourced.pure(_).run))
        }
      }

      implicit val eventedToCommanded: SourcedRule ~~> CommandedRule = new BiNaturalTransformation[SourcedRule, CommandedRule] {
        def apply[A, B](fa: SourcedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B] {
          a => Commanded(\/-(fa.run(a).run))
        }
      }

    }

    sealed trait Instances2 extends Instances1 {

      implicit val functionToValidated: Function1 ~~> ValidatedRule = new BiNaturalTransformation[Function1, ValidatedRule] {
        def apply[A, B](fa: Function1[A, B]): ValidatedRule[A, B] = ValidatedRule[A, B] {
          a => Validated.pure(fa(a))
        }
      }

    }

    sealed trait Instances1 extends Instances0 {
      implicit val functionToEvented: Function1 ~~> SourcedRule = new BiNaturalTransformation[Function1, SourcedRule] {
        def apply[A, B](fa: Function1[A, B]): SourcedRule[A, B] = SourcedRule[A, B] {
          a => Sourced.pure(fa(a))
        }
      }
    }

    sealed trait Instances0 {
      implicit def composed[F[_, _], G[_, _], H[_, _]](implicit FG: F ~~> G, GH: G ~~> H): F ~~> H = GH compose FG
    }
  }

  // Tests

  import Algebra.Instances._
  import Syntax._

  def function = (s: String) => 1

  val validatedRule: ValidatedRule[String, Int] = function.to[ValidatedRule]

  val eventedRule: SourcedRule[String, Int] = function.to[SourcedRule]

  val commandedRule: CommandedRule[String, Int] = function.to[CommandedRule]
  val commandedRule2: CommandedRule[String, Int] = validatedRule.to[CommandedRule]
  val commandedRule3: CommandedRule[String, Int] = eventedRule.to[CommandedRule]

  val rule = ValidatedRule[Int, Int](i => if (i > 0) Validated(\/-(i)) else Validated(-\/(Vector("Bad!"))))

  CommandedRule.identity[Int].andThen {
    (i: Int) => 42
  }.andThenM {
    rule
  }

}


