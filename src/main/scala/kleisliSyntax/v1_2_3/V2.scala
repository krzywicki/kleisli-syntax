package kleisliSyntax.v1_2_3

import scala.language.higherKinds
import scalaz.{-\/, BiNaturalTransformation, \/-, ~~>}

object V2 extends App {

  // -- Building blocks

  trait CompanionOps[G[_, _]] {

    def identity[A](implicit FG: Function1 ~~> G): G[A, A] = FG(x => x)

  }

  // -- Domain

  case class ValidatedRule[A, B](run: A => Validated[B], doc: ValidatedDoc)

  object ValidatedRule extends CompanionOps[ValidatedRule] {

    def create[A, B](error: String)(f: A => Option[B]): ValidatedRule[A, B] = {
      ValidatedRule[A, B](
        a =>  Validated {
            f(a) match {
              case None => -\/(Vector(error))
              case Some(b) => \/-(b)
            }
          },
        ValidatedDoc.single(error)
      )
    }

    def predicate[A](error: String)(p: A => Boolean): ValidatedRule[A, A] = create(error) {
      a => Some(a).filter(p)
    }

  }

  case class IsEvent[E](name: String)

  case class SourcedRule[A, B](run: A => Sourced[B], doc: SourcedDoc)

  object SourcedRule extends CompanionOps[SourcedRule] {

    class CreatePartiallyApplied[A] {
      def apply[E, B](f: A => E)(handler: (A, E) => B)(implicit isEvent: IsEvent[E]): SourcedRule[A, B] = SourcedRule(
        a => {
          val e = f(a)
          val b = handler(a, e)
          Sourced(Vector(e) -> b)
        },
        SourcedDoc.single(isEvent.name)
      )
    }

    def create[A]: CreatePartiallyApplied[A] = new CreatePartiallyApplied[A]

  }

  case class CommandedRule[A, B](run: A => Commanded[B], doc: CommandedDoc)

  object CommandedRule extends CompanionOps[CommandedRule]

  // -- Syntax

  object Syntax {

    class AndThenMPartiallyApplied[A, B, F[_, _], G[_, _]](self: F[A, B]) {

      def apply[C](g: G[B, C])(implicit FG: G ~~> F): F[A, C] = ???

    }

    implicit class Ops[A, B, F[_, _]](val self: F[A, B]) {

      def andThen[C, G[_, _]](g: G[B, C])(implicit FG: G ~~> F): F[A, C] = andThenM[G](g)

      def andThenM[G[_, _]]: AndThenMPartiallyApplied[A, B, F, G] = new AndThenMPartiallyApplied(self)

      def to[G[_, _]](implicit FG: F ~~> G): G[A, B] = FG(self)
    }

  }
  // -- Algebra

  object Algebra {

    object Instances extends Instances3

    sealed trait Instances3 extends Instances2 {

      implicit val validatedToCommanded: ValidatedRule ~~> CommandedRule = new BiNaturalTransformation[ValidatedRule, CommandedRule] {
        def apply[A, B](fa: ValidatedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B](
          a => Commanded(fa.run(a).run.map(Sourced.pure(_).run)),
          CommandedDoc(possibleEvents = Vector.empty, possibleErrors = fa.doc.possibleErrors)
        )
      }

      implicit val eventedToCommanded: SourcedRule ~~> CommandedRule = new BiNaturalTransformation[SourcedRule, CommandedRule] {
        def apply[A, B](fa: SourcedRule[A, B]): CommandedRule[A, B] = CommandedRule[A, B](
          a => Commanded(\/-(fa.run(a).run)),
          CommandedDoc(possibleEvents = fa.doc.possibleEvents, possibleErrors = Vector.empty)
        )
      }

    }

    sealed trait Instances2 extends Instances1 {

      implicit val functionToValidated: Function1 ~~> ValidatedRule = new BiNaturalTransformation[Function1, ValidatedRule] {
        def apply[A, B](fa: Function1[A, B]): ValidatedRule[A, B] = ValidatedRule[A, B](
          a => Validated.pure(fa(a)),
          ValidatedDoc.empty
        )
      }

    }

    sealed trait Instances1 extends Instances0 {
      implicit val functionToEvented: Function1 ~~> SourcedRule = new BiNaturalTransformation[Function1, SourcedRule] {
        def apply[A, B](fa: Function1[A, B]): SourcedRule[A, B] = SourcedRule[A, B](
          a => Sourced.pure(fa(a)),
          SourcedDoc.empty
        )
      }
    }

    sealed trait Instances0 {

      implicit def identityNatTrans[F[_, _]]: F ~~> F = new BiNaturalTransformation[F, F] {
        def apply[A, B](f: F[A, B]): F[A, B] = f
      }

      implicit def composedNatTrans[F[_, _], G[_, _], H[_, _]](implicit FG: F ~~> G, GH: G ~~> H): F ~~> H = GH compose FG
    }
  }

  // Tests

  import Algebra.Instances._
  import Syntax._

  case class FooEvent(value: Int)

  object FooEvent {
    implicit val isEvent: IsEvent[FooEvent] = IsEvent("FooEvent")
  }

  def function = (s: String) => 1

  val validatedRule: ValidatedRule[String, Int] = function.to[ValidatedRule]

  val eventedRule: SourcedRule[String, Int] = function.to[SourcedRule]

  val commandedRule: CommandedRule[String, Int] = function.to[CommandedRule]
  val commandedRule2: CommandedRule[String, Int] = validatedRule.to[CommandedRule]
  val commandedRule3: CommandedRule[String, Int] = eventedRule.to[CommandedRule]

  ValidatedRule.identity[Int] andThen ValidatedRule.identity[Int]

  val first = (identity[Int] _).andThen {
    (i: Int) => i * 2
  }.to[ValidatedRule].andThen {
    ValidatedRule.predicate[Int]("must be positive")(a => a > 0)
  }.to[CommandedRule].andThen {
    SourcedRule.create[Int](value => FooEvent(value))( (value, event) => value + event.value )
  }

}


