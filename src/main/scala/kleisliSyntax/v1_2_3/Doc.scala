package kleisliSyntax.v1_2_3

import scalaz.Monoid

case class SourcedDoc(possibleEvents: Vector[String])

object SourcedDoc {
  val empty = SourcedDoc(Vector.empty)

  def single(possibleEvent: String) = SourcedDoc(Vector(possibleEvent))

  implicit val monoidInstance: Monoid[SourcedDoc] = new Monoid[SourcedDoc] {
    def zero: SourcedDoc = SourcedDoc(Vector.empty)

    def append(f1: SourcedDoc, f2: => SourcedDoc): SourcedDoc = {
      SourcedDoc(f1.possibleEvents ++ f2.possibleEvents)
    }
  }
}

case class ValidatedDoc(possibleErrors: Vector[String])

object ValidatedDoc {
  val empty = ValidatedDoc(Vector.empty)

  def single(possibleError: String) = ValidatedDoc(Vector(possibleError))

  implicit val monoidInstance: Monoid[ValidatedDoc] = new Monoid[ValidatedDoc] {
    def zero: ValidatedDoc = ValidatedDoc(Vector.empty)

    def append(f1: ValidatedDoc, f2: => ValidatedDoc): ValidatedDoc = {
      ValidatedDoc(possibleErrors = f1.possibleErrors ++ f2.possibleErrors)
    }
  }

}

case class CommandedDoc(possibleEvents: Vector[String], possibleErrors: Vector[String])

object CommandedDoc {
  val empty = CommandedDoc(Vector.empty, Vector.empty)

  implicit val monoidInstance: Monoid[CommandedDoc] = new Monoid[CommandedDoc] {
    def zero: CommandedDoc = CommandedDoc(Vector.empty, Vector.empty)

    def append(f1: CommandedDoc, f2: => CommandedDoc): CommandedDoc = {
      CommandedDoc(
        possibleErrors = f1.possibleErrors ++ f2.possibleErrors,
        possibleEvents = f1.possibleEvents ++ f2.possibleEvents
      )
    }
  }
}
