lazy val root = (project in file(".")).
  settings(
    name := "kleisli-syntax",
    scalaVersion := "2.11.11",
    scalacOptions += "-feature",
    scalacOptions += "-language:postfixOps",
    scalacOptions += "-language:higherKinds",
    scalacOptions += "-Ypartial-unification",
    libraryDependencies ++= Seq(
        "org.scalaz" %% "scalaz-core" % "7.2.16",
        "com.chuusai" %% "shapeless" % "2.3.0"
    ),
    addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.4")
  )
